import csv
from os import listdir
from os.path import isfile, join

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db.models import Sum
from django.shortcuts import render

from core.models import ZipValueOverview


def upload(request):
    uploaded_files = []
    fs = FileSystemStorage()

    if request.method == "POST" and request.FILES["uploadfile"]:
        uploadfile = request.FILES["uploadfile"]
        fs.save(uploadfile.name, uploadfile)
        save_to_db(fs.base_url[1:] + uploadfile.name)

    media_path = settings.MEDIA_ROOT
    uploaded_files = [
        f[:-4] for f in listdir(media_path) if isfile(join(media_path, f))
    ]

    return render(request, "upload.html", {"uploaded_files": uploaded_files})


def show_result(request):
    if request.method == "GET":
        summary = request.GET.get("summary")
        if summary is not None:
            summary = True
            zip_value = (
                ZipValueOverview.objects.values("shipping_address_zip")
                .annotate(total_price_with_shipping=Sum("total_price_with_shipping"))
                .order_by("shipping_address_zip")
            )
        else:
            summary = False
            zip_value = ZipValueOverview.objects.all().order_by("shipping_address_zip")

    return render(
        request, "show_view.html", {"zip_value": zip_value, "summary_check": summary}
    )


def save_to_db(file_path):
    with open(file_path, newline="", encoding="iso8859-1") as f:
        csv_reader = csv.DictReader(f, delimiter=";")

        # Skips header
        next(csv_reader)

        for row in csv_reader:
            zip_value = ZipValueOverview()
            zip_value.shipping_address_zip = row["shipping_address_zip"]
            zip_value.total_price_with_shipping = row["total_price_with_shipping"]
            zip_value.save()
