from django.db import models


# Create your models here.
class ZipValueOverview(models.Model):
    shipping_address_zip = models.CharField(
        max_length=5, default="", verbose_name="PLZ"
    )
    total_price_with_shipping = models.IntegerField(default=0, verbose_name="Summe")
