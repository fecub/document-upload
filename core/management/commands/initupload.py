from django.core.management.base import BaseCommand
import os

# Directory
directory = "uploads"


class Command(BaseCommand):
    help = "Creates a upload folder"

    def handle(self, *args, **options):
        isDirExist = os.path.exists(directory)

        if not isDirExist:
            path = os.path.join(".", directory)
            os.mkdir(path)
            print("Directory '% s' created" % directory)
        else:
            print("Directory '% s' exists, not need to create" % directory)
