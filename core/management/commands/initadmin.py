from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    help = "Creates a superuser"

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            username = "root"
            email = ""
            password = "admin"
            print("Creating account for %s (%s)" % (username, email))
            admin = User.objects.create_superuser(
                email=email, username=username, password=password
            )
            admin.is_active = True
            admin.is_admin = True
            admin.is_confirmed = True
            admin.save()
            print(
                f"""superuser for tests created
            username: {username}
            password: {password}
            """
            )
        else:
            print("Admin accounts can only be initialized if no Accounts exist")
