# Sellermath Testaufgabe

- Datei upload
- CSV Daten in die Datenbank transferieren
- Ergebnistabelle anzeigen
- Tabellenspalten sortierbar

## Installation

```bash
$ python -m venv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py initupload
```

If you need a superuser, create it via command

```bash
$ python manage.py initadmin
```

## Run

### With livereload

1. Run livereload

```bash
$ python manage.py livereload
```

2. In another terminal session run django app

```bash
$ python manage.py runserver
```

### Without livereload

Run django app

```bash
$ python manage.py runserver
```
